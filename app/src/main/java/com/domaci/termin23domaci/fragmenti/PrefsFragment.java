package com.domaci.termin23domaci.fragmenti;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.domaci.termin23domaci.R;

public class PrefsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);
    }
}
