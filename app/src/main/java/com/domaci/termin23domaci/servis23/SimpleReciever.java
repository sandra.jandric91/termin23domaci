package com.domaci.termin23domaci.servis23;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.domaci.termin23domaci.R;
import com.domaci.termin23domaci.activities.SecondActivity;
import com.domaci.termin23domaci.tools.ReviewerTools;

import static com.domaci.termin23domaci.App.CHANNEL_ID;

public class SimpleReciever extends BroadcastReceiver {

    private static final int NOTIFICATION_ID = 1;
    private static final int NOTIFICATION_ID1 = 2;
    private static final int NOTIFICATION_ID2 = 3;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("KOMENTAR")) {
            String komentarZaNotif = intent.getExtras().getString("KOMENTAR_ZA_NOTIF");
            prepareNotification(komentarZaNotif, context);
        }
    }

    private void prepareNotification(String resultCode, Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID);

        if (resultCode == ReviewerTools.getConnectionType(0)) {
            builder.setSmallIcon(R.drawable.ic_no_connection);
            builder.setContentTitle("Internet problem");
            builder.setContentText("Niste povezani na internet");
            notificationManager.notify(NOTIFICATION_ID, builder.build());
        } else if (resultCode == ReviewerTools.getConnectionType(1)) {
            String text = resultCode;
            builder.setSmallIcon(R.drawable.ic_wifi_connection);
            builder.setContentTitle("Internet wifi");
            builder.setContentText(text);
            notificationManager.notify(NOTIFICATION_ID1, builder.build());
        } else if (resultCode == ReviewerTools.getConnectionType(2)) {
            String text = resultCode;
            builder.setSmallIcon(R.drawable.ic_mobile_connection);
            builder.setContentTitle("Internet data");
            builder.setContentText(text);
            notificationManager.notify(NOTIFICATION_ID2, builder.build());
        }
    }
}
