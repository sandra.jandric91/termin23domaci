package com.domaci.termin23domaci.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.domaci.termin23domaci.R;
import com.domaci.termin23domaci.servis23.KomentarService;
import com.domaci.termin23domaci.tools.ReviewerTools;

public class SecondActivity extends AppCompatActivity {

    private TextView tvIme;
    private EditText etIme;
    private TextView tvKomentar;
    private EditText etKomentar;
    private Button bOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        tvIme = findViewById(R.id.tvIme);
        etIme = findViewById(R.id.etIme);
        tvKomentar = findViewById(R.id.tvKomenetar);
        etKomentar = findViewById(R.id.etKomentar);
        bOK = findViewById(R.id.bOK);
        bOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int comment = ReviewerTools.getConnectivityStatus(getApplicationContext());
                if (comment == ReviewerTools.TYPE_WIFI) {
                    String komentarPoruka = etKomentar.getText().toString();
                    Intent intent = new Intent(SecondActivity.this, KomentarService.class);
                    intent.putExtra(KomentarService.KOMENTAR, komentarPoruka);
                    startService(intent);
                } else if (comment == ReviewerTools.TYPE_MOBILE) {
                    String komentarPoruka = etKomentar.getText().toString();
                    Intent intent = new Intent(SecondActivity.this, KomentarService.class);
                    intent.putExtra(KomentarService.KOMENTAR, komentarPoruka);
                    startService(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Uredjaj nije povezan na internet", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
